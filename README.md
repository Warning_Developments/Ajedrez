## Chess

	Este juego de Ajedrez es el primer proyecto que estamos abarcando como grupo de desarrolladores en Warning_Developments.

## INSTRUCCIONES:

	Para usar el juego:
		- Abrir la terminal.
		- Ubicarse en la carpeta del directorio.
		- Escribir "make" la primera vez que lo vayan a jugar.
		- Para que empiece el juego solo escriban "make start".

## DEVELOPERS:

	make - Compila el juego.
	make start - Compila y empieza el juego.
	make clean - Borra los archivos objeto (.o) y el ejecutable "chess".
	make debug - Corre el juego con el GDB.
	make run_valgrind - Checkea la memoria dinamica del juego.

## ATRIBUCIONES y COLABORACIONES:

	® Lucario - Developer.
	® 00santiagob - Developer.

#include <iostream>
#include <cstdlib>
#include <utility>  // Incluye la función make_pair.
#include <cassert>
#include "TADPiece.h"

using namespace std;

struct _piece_t{
    colour_t color;
    name_t nombre;
};

struct _box_t{
    position xy;
    piece_t pieza;
    bool ocup;
};

struct _board_t {
    box_t tablero[8][8];
};

piece_t init_piece(colour_t c, name_t n){
    piece_t piece = (piece_t)malloc(sizeof(struct _piece_t));
    assert(piece != NULL);
    piece->color = c;
    piece->nombre = n;
    return piece;
}

void dump_piece(board_t tabl, position xy1){
    name_t n;
    colour_t c;
    assert(tabl != NULL);
    n = tabl->tablero[xy1.first][xy1.second]->pieza->nombre;
    c = tabl->tablero[xy1.first][xy1.second]->pieza->color;
    if(c == blanco){
        if(n == rey){cout << "La pieza seleccionada es: Rey - Kb" << endl;}
        if(n == reina){cout << "La pieza seleccionada es: Reina - Qb" << endl;}
        if(n == alfil){cout << "La pieza seleccionada es: Alfil - Ab" << endl;}
        if(n == caballo){cout << "La pieza seleccionada es: Caballo - Cb" << endl;}
        if(n == torre){cout << "La pieza seleccionada es: Torre - Tb" << endl;}
        if(n == peon){cout << "La pieza seleccionada es: Peon - Pb" << endl;}
    }
    else if(c == negro){
        if(n == rey){cout << "La pieza seleccionada es: Rey - Kn" << endl;}
        if(n == reina){cout << "La pieza seleccionada es: Reina - Qn" << endl;}
        if(n == alfil){cout << "La pieza seleccionada es: Alfil - An" << endl;}
        if(n == caballo){cout << "La pieza seleccionada es: Caballo - Cn" << endl;}
        if(n == torre){cout << "La pieza seleccionada es: Torre - Tn" << endl;}
        if(n == peon){cout << "La pieza seleccionada es: Peon - Pn" << endl;}
    }
}

void move_piece(board_t tabl, position xy1, position xy2){
    tabl->tablero[xy2.first][xy2.second]->pieza = tabl->tablero[xy1.first][xy1.second]->pieza;
    tabl->tablero[xy2.first][xy2.second]->ocup = true;
    tabl->tablero[xy1.first][xy1.second]->pieza = NULL;
    tabl->tablero[xy1.first][xy1.second]->ocup = false;
}

box_t init_box(position pos){
    box_t casilla = (box_t)malloc(sizeof(struct _box_t));
    assert(casilla != NULL);
    casilla->xy = pos;
    casilla->pieza = NULL;
    casilla->ocup = false;
    return casilla;
}

position selected_piece(board_t tabl, colour_t c, int x, int y){
    position pos;
    bool check = true;
    if(c == blanco){
        cout << "Juega el BLANCO" << endl;
        while(check){
            cout << "Posicion de la pieza a mover: ";
            cin >> x >> y;
            if (0<x && x<9 && 0<y && y<9){
                pos = make_pair(x-1,y-1);
                if(tabl->tablero[pos.first][pos.second]->ocup == true){
                    dump_piece(tabl, pos);
                    check = false;
                }
                else{
                     system("clear");
                    dump_board(tabl);
                    cout << "Juega el BLANCO" << endl;
                    cout << "La posicion seleccionada esta vacia." << endl;
                }
            }  
            else{
                system("clear");
                dump_board(tabl);
                cout << "Juega el BLANCO" << endl;
                cout << "La posicion es incorrecta, intentelo nuevamente." << endl;
            }
        }
    }
    else if(c == negro){
        cout << "Juega el NEGRO" << endl;
        while(check){
            cout << "Posicion de la pieza a mover: ";
            cin >> x >> y;
            if (0<x && x<9 && 0<y && y<9){
                pos = make_pair(x-1,y-1);
                if(tabl->tablero[pos.first][pos.second]->ocup == true){
                    dump_piece(tabl, pos);
                    check = false;
                }
                else{
                     system("clear");
                    dump_board(tabl);
                    cout << "Juega el NEGRO" << endl;
                    cout << "La posicion seleccionada esta vacia." << endl;
                }
            }  
            else{
                system("clear");
                dump_board(tabl);
                cout << "Juega el NEGRO" << endl;
                cout << "La posicion es incorrecta, intentelo nuevamente." << endl;
            }
        }
    }
    return pos;
}

position new_position(board_t tabl, int x, int y){
    position pos;
    bool check = true;
    while(check){
        cout << "Nueva posicion: ";
        cin >> x >> y;
        if (0<x && x<9 && 0<y && y<9){
            pos = make_pair(x-1,y-1);
            check = false;
        }  
        else{
            system("clear");
            dump_board(tabl);
            cout << "La posicion es incorrecta, intentelo nuevamente." << endl;
        }
    }
    return pos;
}


board_t init_board(){
    board_t tabl = (board_t)malloc(sizeof(struct _board_t));
    assert(tabl != NULL);
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            tabl->tablero[i][j] = init_box(make_pair(i,j));
        }
    }
    tabl->tablero[0][0]->pieza = init_piece(negro,torre);
    tabl->tablero[0][0]->ocup = true;
    tabl->tablero[0][1]->pieza = init_piece(negro,alfil);
    tabl->tablero[0][1]->ocup = true;
    tabl->tablero[0][2]->pieza = init_piece(negro,caballo);
    tabl->tablero[0][2]->ocup = true;
    tabl->tablero[0][3]->pieza = init_piece(negro,rey);
    tabl->tablero[0][3]->ocup = true;
    tabl->tablero[0][4]->pieza = init_piece(negro,reina);
    tabl->tablero[0][4]->ocup = true;
    tabl->tablero[0][5]->pieza = init_piece(negro,caballo);
    tabl->tablero[0][5]->ocup = true;
    tabl->tablero[0][6]->pieza = init_piece(negro,alfil);
    tabl->tablero[0][6]->ocup = true;
    tabl->tablero[0][7]->pieza = init_piece(negro,torre);
    tabl->tablero[0][7]->ocup = true;
    for(int j = 0; j < 8; j++){
        tabl->tablero[1][j]->pieza = init_piece(negro,peon);
        tabl->tablero[1][j]->ocup = true;
        tabl->tablero[6][j]->pieza = init_piece(blanco,peon);
        tabl->tablero[6][j]->ocup = true;
    }
    tabl->tablero[7][0]->pieza = init_piece(blanco,torre);
    tabl->tablero[7][0]->ocup = true;
    tabl->tablero[7][1]->pieza = init_piece(blanco,alfil);
    tabl->tablero[7][1]->ocup = true;
    tabl->tablero[7][2]->pieza = init_piece(blanco,caballo);
    tabl->tablero[7][2]->ocup = true;
    tabl->tablero[7][3]->pieza = init_piece(blanco,rey);
    tabl->tablero[7][3]->ocup = true;
    tabl->tablero[7][4]->pieza = init_piece(blanco,reina);
    tabl->tablero[7][4]->ocup = true;
    tabl->tablero[7][5]->pieza = init_piece(blanco,caballo);
    tabl->tablero[7][5]->ocup = true;
    tabl->tablero[7][6]->pieza = init_piece(blanco,alfil);
    tabl->tablero[7][6]->ocup = true;
    tabl->tablero[7][7]->pieza = init_piece(blanco,torre);
    tabl->tablero[7][7]->ocup = true;
    return tabl;
}

void dump_board(board_t t){
    piece_t piece = NULL;
    assert(t != NULL);
    cout << "---- Chess - Game Started ----" << endl << endl << endl;
    for(int i = 0; i < 8; i++){
        cout << "   |----|----|----|----|----|----|----|----|" << endl;
        cout << i+1 << "  ";
        for(int j = 0; j < 8 ; j++){cout << "|";
            piece = t->tablero[i][j]->pieza;
            if(piece != NULL){
                if(piece->color == negro){
                    if(piece->nombre == peon){cout <<" Pn ";}
                    if(piece->nombre == torre){cout <<" Tn ";}
                    if(piece->nombre == rey){cout <<" Kn ";}
                    if(piece->nombre == reina){cout <<" Qn ";}
                    if(piece->nombre == caballo){cout <<" Cn ";}
                    if(piece->nombre == alfil){cout <<" An ";}
                }
                else if(piece->color == blanco){
                    if(piece->nombre == peon){cout <<" Pb ";}
                    if(piece->nombre == torre){cout <<" Tb ";}
                    if(piece->nombre == rey){cout <<" Kb ";}
                    if(piece->nombre == reina){cout <<" Qb ";}
                    if(piece->nombre == caballo){cout <<" Cb ";}
                    if(piece->nombre == alfil){cout <<" Ab ";}
                }
            }
            else {cout << "    ";
            }
        }
        cout << "|" << endl;
    }
    cout << "   |----|----|----|----|----|----|----|----|" << endl;
    cout << "     1    2    3    4    5    6    7    8   " << endl << endl;
}

void free_board(board_t tabl){
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            free(tabl->tablero[i][j]->pieza);
            free(tabl->tablero[i][j]);
        }
    }
    free(tabl);
    tabl = NULL;
}

CC = g++
CCFLAGS = -Wall -Werror -Wextra -g
OBJECTS = TADPiece.o main.o

all: chess
%.o: %.c
	$(CC) $(CCFLAGS) -c -o $@ $^

chess: $(OBJECTS)
	$(CC) $(CCFLAGS) -o $@ $^

start: chess
	./chess

run_valgrind: chess
	valgrind --show-reachable=yes --leak-check=full ./chess

debug: chess
	gdb ./chess

clean:
	rm -f chess $(OBJECTS)

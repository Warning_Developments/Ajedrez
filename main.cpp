#include <iostream>
#include <cstdlib>
#include <utility>
#include "TADPiece.h"

using namespace std;

int main(){
    system("clear");
    board_t tablero = init_board();
    int x,y,x2,y2;
    position pos, pos2;
    colour_t c;
    bool k = true;
    name_t name;
    while(k){
        dump_board(tablero);
        c = blanco;
        pos = selected_piece(tablero, c, x, y);
        pos2 = new_position(tablero, x2, y2);
        move_piece(tablero, pos, pos2);
        system("clear");
        dump_board(tablero);
        c = negro;
        pos = selected_piece(tablero, c, x, y);
        pos2 = new_position(tablero, x2, y2);
        move_piece(tablero, pos, pos2);
        k = false;
    }
    free_board(tablero);
    return 0;
}